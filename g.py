#Draw a circle
from matplotlib import pyplot as plt
import numpy as np

x = 1024
y = 1024
r = 400.0
middle = 1024 / 2

A = np.zeros((x,y))

for i in range(0, x - 1):
	for j in range(0, y - 1):
		x_len = np.absolute(middle - i)
		y_len = np.absolute(middle - j)
		len_from_middle = np.hypot(x_len,y_len)
		if(len_from_middle <= r):
			A[i,j] = np.cos(len_from_middle / r)

plt.imshow(A, cmap=plt.cm.bone)
plt.show()