#Print Trapezium Area From list of data
import math

def trapezium(a,b,h):
	Area = (a+b)*h/2
	return Area

def loop_through_list(x_list, y_list):
	l = len(x_list)
	if l != len(y_list):
		print("Error")

	for i in xrange(0,l / 2):
		print(i)
		print(trapezium(y_list[i], y_list[i+1], math.fabs(x_list[i] - x_list[i+1])))
		i += 1

#x = [1,2,3,4,5,6]
#y = [2,3,4,5,6,7]
loop_through_list(x,y)
		
