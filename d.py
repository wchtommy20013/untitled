#Print 1-99 in English
import random

word_list = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
word_list_special_case = ['ten', 'eleven', 'twelve', 'thirdteen', 'fourteen', 'fifteen']
word_list_tens = ['twenty', 'thirdty', 'fourty', 'fifty']

def tens(num):
	if(num / 10 < 5):
		return word_list_tens[(num / 10) - 2]
	elif(num /10 == 8):
		return 'eighty'
	else:
		return word_list[num / 10 - 1] + "ty"


def word(num):
	if(num < 10):
		return word_list[num - 1]
	elif(num <= 15):
		return word_list_special_case[num - 10]
	elif(num < 20):
		return word_list[(num % 10) - 1] + 'teen'
	elif(num % 10 == 0):
		return tens(num)
	else:
		return tens(num) + '-' + word_list[num%10-1]

#num = randint(1,99)
for num in xrange(1,99):
	print(word(num)) 





