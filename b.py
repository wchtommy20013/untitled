#Root Sum Square
def rootsumsquare( input_x, input_y ):
	tmp_x = input_x ** input_x;
	tmp_y = input_y ** input_y;
	answer_z = (tmp_x + tmp_y)^(1/2)
	return answer_z

some_input_x = 1
some_input_y = 2
returned_result_z = rootsumsquare(some_input_x, some_input_y) 
print(returned_result_z)

print(rootsumsquare(3,4))
