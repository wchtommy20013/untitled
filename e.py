#Pascal Triangle
import random
import numpy as np

def triangle(n):
	x = np.zeros((n,n))
	x[:,0] = 1
	for i in range(1,n):
		x[i,i] = 1
		for j in range(1,i):
			x[i,j] = x[i-1,j] + x[i-1,j-1]
	return x

print(triangle(10))
